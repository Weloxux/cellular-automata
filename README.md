Cellular automata dungeon generator
===================================

A prototype dungeon generator in Lua using the cellular automata method.

Ludum Dare post [here](http://ludumdare.com/compo/2015/08/17/cellular-automata-dungeon-generator/)

Examples
--------
Wall if more than five adjescent walls, space if less than three adjescent.
Unchanged if three, four, or five adjescent walls.
Cellulated 1 time.
```
#####  ##   ### #  #######################    #####  #   # #
#####  ##  ###    #   ###  ####### ######    ######  #  # ##
### # ###  ##     ##   ##     #    #####      ###### #### ##
##   ######        #######   #    ######         ###  #    #
#     ####### ###  #  ###   ##     ####                #   #
##     ##########  #  ##   #      ######           #       #
###    #########  #######   #   #########         ###       
####   ########## #######  ##    ####  #           # #      
###    ###  #####  ##########    ####  ## ###  ## ###       
##           ###  ###  #####     #### ######   ######       
##            ##  ##     ###      #########      ####      #
##                #       #    ## ########   ##  #####    ##
##                #            ### #######  #### # ###### ##
#        ##     #####  ###    ###  #######   ####  ##### ###
###      ###   ####### ##### #### #########     #  #####   #
###     ####   ####### ##### ######  ##### #        #####  #
#       # #    #####   ###### ####    ###  ## #    # #######
    ## #       ######   # ###   #     ##  ######    # ######
  # ####    #########  #   ##        ### ## #      #   #    
#### ###     #   #   ###  # ###    #######   #      # #     
##  # ###             #####   ##    #  ####    ##    ### ###
###    ####   #        #  #         #   ###   ####   #######
##    ####   ###    ###  #          ##  ##   #####    ## ###
##     #    #####       ####        # #  #    ######  ##   #
# #        #####        ####  ##  ##  ### #  #### ##  ###  #
##        ######        #  ### ####### ### #####       ##  #
#          ## ####          ############## ###  ##         #
#     ## #     ###        ########## #####  ##  #   ##     #
#    ######       #      ##########   ### #######    ##  ###
## # ######     ###  # ############  #############  ########
```

Wall if more than five adjescent walls, space if less than three adjescent.
Unchanged if three, four, or five adjescent walls.
Cellulated 900 times.
```
#######################################  #############   ###
##  #########      ####################  ######## ####   ###
##  #######         ####### ### # ###   ########   ###      
###########        ######    #     #     #######   ####     
###########      ########   #             ###############  #
####  #  #      #####      ###             ######## ########
###            ######       #     ##       ##   ##   #######
###     ##    ########     #     ####   #   #   ###    #####
##      ##   ########      ##  #####   ### ##  ####    #####
##       #######   #     ###   #####    ## ##  #####    ####
#####    #######        ####    ###    ###  #  #####     ###
#######   #######       ####    #     ###   ########      ##
##  ####    #####        #### ##      ####  ####  ###    ###
#   ####    ######        ##  ##      ##########  ####    ##
   ########  ####  #         ###       ##############      #
# #########  ###  ###        #         ###   ########     ##
  ########## #### ##        ##          #      ########   ##
   ##########    ###      ####                 ##  #####   #
  #   ######    #####    #####  ###           ###  #####   #
####  #####      ###     ####### ##           ###  ###      
###########              ######              ###   ###      
###########          ## #######     #        #### ###### ###
  #######      # #   ############# ### ###   ############ ##
  #######     #####  ########  #### #######  ############   
#########    #####   ########   ##     ###################  
############ ####    #########         ################### #
########## ## #     ##  ######        #################### #
#########          ###  ######         ########## ######    
##  ######       #####  ######        ##########           #
##  ##########   ############## #   #############   # # ####
```

Wall if more than four adjescent walls, space if less than three adjescent.
Unchanged if three or four adjescent walls.
Cellulated 900 times.
```
############################################################
############################################################
########################################  ##################
#######################################    #################
#######################################    #################
########################################  ##################
############################################################
############################################################
############################################################
############################################################
############################################################
############################################################
############################################################
############################################################
############################################################
############################################################
##########   ###############################################
#########     ##############################################
########      ##############################################
########     ###############################################
#########   ################################################
############################################################
#########################################    ###############
########################################      ##############
#######################################       ##############
#######################################       ##############
#######################################      ###############
########################################    ################
############################################################
############################################################
```

Wall if more than five adjescent walls, space if less than four adjescent.
Unchanged if four or five adjescent walls.
Cellulated 900 times.
```
#####################  #############   #####  #########     
####################   ######## ####    ###   #######       
######### ###   ###    #######   ##            ######      #
#######                ######                   ####       #
######                  ####                              ##
###                      ##                              ###
###                      ##                             ####
###                       #   ##       ##              #####
###                      ##  ####     ####             ###  
##                       ##  #####    ########          ##  
#                         #  #####     ########         ####
##                        ########      ####  ###        ###
##                        ####  ###           ####       ###
#                          ##   ###           #####      ###
##                             ####           ######     ###
##                            ####             ######     ##
##                           ####               #####      #
##                           ##                 #####       
##                          ###                 ####        
#                           ###              #######        
#                          ###              ########        
#                          ####             #######        #
#                          #####            #######       ##
#       ##                 ######           #######       ##
       ####                #######         #########       #
       #####               ########        #########        
       #####             ##########         ## ####         
        ###             ####### ##              ##         #
#      ####             ######                             #
#    ########           #######       ##     #####    ###  #
```

