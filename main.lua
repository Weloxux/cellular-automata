-- Dungeon gen test
-- Copyright (C) 2015 Marnix Massar <marnix@vivesce.re>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

dungeon = {}
sorts = {"wall", "space"}
seed = os.time()

local function init()
    for i = 1, 30 do
        local newrow = {}

        for n = 1, 60 do
            seed = seed + 11
            math.randomseed(seed)
            local newtile = {sort = sorts[math.random(#sorts)]}
            table.insert(newrow, newtile)
        end

        table.insert(dungeon, newrow)
    end
end

function check(pos)
    if pos ~= nil and pos.sort == "wall" then -- If this tile is a wall then add one to count
        return 1
    elseif pos == nil then -- If this tile doesn't exist (and thus counts as a wall) then add one to count
        return 1
    else -- If this tile is open, add zero
        return 0
    end
end

local function cellulate(times) -- the cellulate function running multiple times gives smoother maps
    for i = 1, times do
        for k1,v1 in pairs(dungeon) do
            for k2,v2 in pairs(v1) do
                count = 0

                count = count + check(v1[k2 - 1]) -- left
                count = count + check(v1[k2 + 1]) -- right
    
                if k1 == 1 then -- top row
                    count = count + 3
                else
                    count = count + check(dungeon[k1 - 1][k2 - 1]) -- top and left
                    count = count + check(dungeon[k1 - 1][k2]) -- directly above
                    count = count + check(dungeon[k1 - 1][k2 + 1]) -- top and right
                end

                if k1 == #dungeon then -- bottom row
                    count = count + 3
                else
                    count = count + check(dungeon[k1 + 1][k2 - 1])
                    count = count + check(dungeon[k1 + 1][k2])
                    count = count + check(dungeon[k1 + 1][k2 + 1])
                end

                if count > 5 then -- apply rules to count - toy around with these for different results
                    v2.sort = "wall"
                elseif count < 3 then
                    v2.sort = "space"
                end
            end
        end
    end
end

local function draw()
    for k1,v1 in pairs(dungeon) do
        for k2,v2 in pairs(v1) do
            if v2.sort == "wall" then
                io.write("#")
            else
                io.write(" ")
            end
        end
        io.write("\n")
    end
end

local function main()
    init()
--    Uncomment debug line below to see original completely random map
--    draw()
    cellulate(1)
    draw()
end

main()
